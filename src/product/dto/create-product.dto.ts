import { IsNotEmpty, Length } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 32)
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  category: 'Drink' | 'Bakery ' | 'Others';
}
